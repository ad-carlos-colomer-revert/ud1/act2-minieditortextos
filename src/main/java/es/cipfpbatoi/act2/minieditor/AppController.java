package es.cipfpbatoi.act2.minieditor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author carlo
 */
public class AppController implements Initializable {

    @FXML
    private TextArea taEditor;
    @FXML
    private Label lblInfo;
    @FXML
    private Button btnAbrir;
    @FXML
    private Button btnCerrar;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnNuevo;

    private Stage escenario;
    private File f, lastPath;
    private FileChooser fileChooser;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // inicializaciones del controlador
        escenario = null;
        fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        taEditor.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                lblInfo.setText("Lineas: " + String.valueOf(taEditor.getText().split("\n").length) + " Caracteres: "
                        + String.valueOf(taEditor.getText().chars().count()));
            }
        });

    }

    @FXML
    private void handleNuevo() {

        if (f == null && taEditor.isDisabled()) {
            lblInfo.setText("Creando nuevo fichero");
            taEditor.setDisable(false);
        } else {
            Alert alert = new Alert(AlertType.CONFIRMATION, "Desea guardar el fichero?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                System.out.println("fichero guardado");
                handleGuardar();
            } else if (alert.getResult() == ButtonType.NO) {
                taEditor.clear();
                f = null;
            }
        }

    }

    @FXML
    private void handleAbrir() throws IOException {

        if (!taEditor.isDisabled()) {
            Alert alert = new Alert(AlertType.CONFIRMATION, "Desea guardar el fichero?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                System.out.println("fichero guardado");
                handleGuardar();
                abrirFichero();
            } else if (alert.getResult() == ButtonType.NO) {
                abrirFichero();
            }

        } else {
            abrirFichero();
        }

    }

    private void abrirFichero() {

        if (lastPath != null) {
            fileChooser.setInitialDirectory(lastPath);
        }
        f = fileChooser.showOpenDialog(escenario);

        if (f != null) {
            lastPath = f.getParentFile();

            lblInfo.setText("Abriendo un fichero");
            String texto = "";
            try {
                Path p = f.toPath();
                List<String> text = Files.readAllLines(p);

                for (String t : text) {

                    texto = texto + t + "\n";

                }
                taEditor.setDisable(false);
                taEditor.clear();
            } catch (Exception e) {
                taEditor.appendText(e.toString());
            }

            taEditor.appendText(texto);

        }
    }

    @FXML
    private void handleGuardar() {

        if (!taEditor.isDisabled()) {

            if (f != null) {
                Path p = f.toPath();
                try {

                    List<String> texto = Arrays.asList(taEditor.getText());
                    Files.write(p, texto);

                } catch (Exception e) {
                    taEditor.appendText(e.toString());
                } finally {
                    try {
                        taEditor.setDisable(true);
                        taEditor.clear();
                        f = null;

                    } catch (Exception e2) {
                        taEditor.appendText(e2.toString());
                    }
                }

            } else {

                if (lastPath != null) {
                    fileChooser.setInitialDirectory(lastPath);
                }
                f = fileChooser.showSaveDialog(escenario);
                if (f != null) {
                    Path p = f.toPath();
                    try {

                        List<String> texto = Arrays.asList(taEditor.getText());
                        Files.write(p, texto);

                    } catch (Exception e) {
                        taEditor.appendText(e.toString());
                    } finally {
                        try {
                            taEditor.setDisable(true);
                            taEditor.clear();
                            f = null;

                        } catch (Exception e2) {
                            taEditor.appendText(e2.toString());
                        }
                    }

                }

            }
            lblInfo.setText("Fichero guardado");
        }

    }

    @FXML
    private void handleCerrar() {

        if (f != null || !taEditor.isDisabled()) {
            lblInfo.setText("Cerrando fichero");
            Alert alert = new Alert(AlertType.CONFIRMATION, "Desea guardar el fichero antes de cerrarlo?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                System.out.println("fichero guardado");
                handleGuardar();
            } else if (alert.getResult() == ButtonType.NO) {
                taEditor.clear();
                taEditor.setDisable(true);
                f = null;
            }
            lblInfo.setText("Fichero cerrado");
        } else {
            lblInfo.setText("Cerrando programa");
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Salir");
            alert.setHeaderText("Vas a salir. ");
            alert.setContentText("Estás deacuerdo?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                System.exit(0);
            }
        }

    }

    void setEscenario(Stage escenario) {
        this.escenario = escenario;
    }

}
